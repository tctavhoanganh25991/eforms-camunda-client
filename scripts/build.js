// Require
const spawn = require("cross-spawn");
const cpr = require("child_process");
const fse = require("fs-extra");
const os = require("os");
const _ = console.log;

const curDir = __dirname;
const projectDir = `${curDir}/..`
const serverDir = `${curDir}/../camunda-server`

// Currently support window
const isWin = os.platform() === "win32";
if(!isWin) return _("Just quick build for windows")


/* Step 1. Build Angular */
const buildAngular = (cb) => {
	const buildCpr = spawn("npm", ["run", "_build"], {stdio: "inherit"})
	buildCpr.on('close', closingCode => cb(closingCode));
}

// Copy file
const updateCamundaClient = () => {
	const srcPath = `${projectDir}/target/webapp/app`
	const distPath = `${serverDir}/server/apache-tomcat-8.0.47/webapps/camunda/app`
	fse.copySync(srcPath, distPath)
}


/* Step 2. Update built files to Camunda Client */
const buildCb = (closingCode) => {
		// Sanity check
		const success = closingCode === 0;
	    if(!success) return _("Fail to build, so doesnt run copy");
		
		// Copy built file
		_("[INFO] Copying... built files to Camunda Client")
	    updateCamundaClient();
	    _("[INFO] Copy finish")
	}

// Run
const run = () => {
	buildAngular(buildCb)
}

run()