// Require
const spawn = require("cross-spawn");
const cpr = require("child_process");
const os = require("os");
const _ = console.log

// Currently support window
const isWin = os.platform() === "win32";
if(!isWin) return _("Just quick start for windows")

// Run cmd
const curDir =  __dirname;
const serverDir = `${curDir}/../camunda-server`;
const startCamunda = `cd ${serverDir} && start-camunda.bat`

cpr.exec(startCamunda, (error, stdout, stderr) => {
	_(stdout)
});
