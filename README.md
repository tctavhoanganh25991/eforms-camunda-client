## Development Environment

### Setup
#### Prerequisite
Need JAVA_HOME, JRE_HOME environment variables

|   | OS      | Steps                                                             |
|---|---------|-------------------------------------------------------------------|
| 1 | Windows | Search "Edit system environment variables", follow wizard to edit |
| 2 | Linux   | Using cmd, export [VARIABLE_NAME]=[VARAIABLE_VALUE]               |


+ Example

|   | Name      | Path                               |
|---|-----------|------------------------------------|
| 1 | JAVA_HOME | C:\Program Files\Java\jdk1.8.0_151 |
| 2 | JRE_HOME  | C:\Program Files\Java\jre1.8.0_151 |

### Run
Have to run Camunda Server (serve both backend & client UI)

Rebuild Angular code after update

##### Install dependencies
```
npm install
```
##### Start server
```
npm run start
```
##### Build Angular code
```
npm run build
```